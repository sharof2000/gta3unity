﻿using UnityEngine;
using UnityEditor;
using System.Collections;

public class SceneCameraWatch : MonoBehaviour
{
	public const float MAX_TIME_DELTA = 1f / 10f;

	public float lerpSpeed = 8f;

	// Use this for initialization
	void Start()
	{
	}

	// Update is called once per frame
	void Update()
	{
		if ( SceneView.lastActiveSceneView )
		{
			float dt = Mathf.Min( Time.deltaTime, MAX_TIME_DELTA );

			Vector3 targetPosition = transform.position;
			Vector3 sceneCamPos = SceneView.lastActiveSceneView.pivot;
			Vector3 delta = targetPosition - sceneCamPos;
			sceneCamPos += delta * lerpSpeed * dt;
			
			SceneView.lastActiveSceneView.pivot = sceneCamPos;
			SceneView.lastActiveSceneView.Repaint();
		}
	}
}