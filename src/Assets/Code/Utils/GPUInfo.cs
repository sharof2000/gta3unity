﻿using UnityEngine;
using System.Collections;

public class GPUInfo : MonoBehaviour
{
    // Use this for initialization
    void Start()
    {
        print( "SystemInfo.graphicsDeviceID: " + SystemInfo.graphicsDeviceID );
        print( "SystemInfo.graphicsDeviceName: " + SystemInfo.graphicsDeviceName );
        print( "SystemInfo.graphicsDeviceVendor: " + SystemInfo.graphicsDeviceVendor );
        print( "SystemInfo.graphicsDeviceVendorID: " + SystemInfo.graphicsDeviceVendorID );
        print( "SystemInfo.graphicsDeviceVersion: " + SystemInfo.graphicsDeviceVersion );
        print( "SystemInfo.graphicsMemorySize: " + SystemInfo.graphicsMemorySize );
        print( "SystemInfo.graphicsPixelFillrate: " + SystemInfo.graphicsPixelFillrate );
        print( "SystemInfo.graphicsShaderLevel: " + SystemInfo.graphicsShaderLevel );
    }

    // Update is called once per frame
    void Update()
    {

    }
}