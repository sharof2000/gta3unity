using UnityEngine;
using System.Collections;

public static class MeshUtils
{
	public const int DEFAULT_PIXELS_PER_UNIT = 256;

	public static Mesh CreateDoomWall( Vector2 v1, Vector2 v2, float floorHeight, float ceilingHeight, Color color, int pixelsPerWorldUnit = DEFAULT_PIXELS_PER_UNIT )
	{
		Mesh mesh = new Mesh();

		float uv_scale = 1f / ( float ) pixelsPerWorldUnit;

		Vector3[] vertices = new Vector3[ 4 ];
		Color[] colors = new Color[ 4 ];
		int[] triangles = new int[ 2 * 3 ];
		Vector2[] uv = new Vector2[ 4 ];
		Vector2[] uv2 = new Vector2[ 4 ];

		vertices[ 0 ] = new Vector3( v1.x, floorHeight, v1.y );
		vertices[ 1 ] = new Vector3( v2.x, floorHeight, v2.y );
		vertices[ 2 ] = new Vector3( v1.x, ceilingHeight, v1.y );
		vertices[ 3 ] = new Vector3( v2.x, ceilingHeight, v2.y );

		colors[ 0 ] = color;
		colors[ 1 ] = color;
		colors[ 2 ] = color;
		colors[ 3 ] = color;

		triangles[ 0 ] = 0;
		triangles[ 1 ] = 2;
		triangles[ 2 ] = 3;
		triangles[ 3 ] = 0;
		triangles[ 4 ] = 3;
		triangles[ 5 ] = 1;

		uv[ 0 ] = uv2[ 0 ] = new Vector2( 0f, 0f );
		uv[ 1 ] = uv2[ 1 ] = new Vector2( 1f, 0f );
		uv[ 2 ] = uv2[ 2 ] = new Vector2( 0f, 1f );
		uv[ 3 ] = uv2[ 3 ] = new Vector2( 1f, 1f );

		mesh.vertices = vertices;
		mesh.colors = colors;
		mesh.triangles = triangles;
		mesh.uv = uv;
		mesh.uv2 = uv2;

		mesh.RecalculateNormals();
		mesh.RecalculateBounds();
#if UNITY_EDITOR
		//Unwrapping.GenerateSecondaryUVSet( mesh );
#endif

		return mesh;
	}

	public static Mesh CreateSolidConvexPolygon( Vector2[] convexVertices, Color color, AxisPlane plane, bool flipNormals = false, int pixelsPerWorldUnit = DEFAULT_PIXELS_PER_UNIT )
	{
		if ( convexVertices.Length < 3 )
		{
			throw new UnityException( "����� �� ������� ���� 3 �������" );
		}

		Mesh mesh = new Mesh();

		float uv_scale = 1f / ( float ) pixelsPerWorldUnit;

		Vector3[] vertices = new Vector3[ convexVertices.Length ];
		Color[] colors = new Color[ convexVertices.Length ];
		int[] triangles = new int[ ( convexVertices.Length - 2 ) * 3 ];
		Vector2[] uv = new Vector2[ convexVertices.Length ];
		Vector2[] uv2 = new Vector2[ convexVertices.Length ];

		triangles[ 0 ] = 0;
		triangles[ 1 ] = 1;

		for ( int i = 0; i < convexVertices.Length; i++ )
		{
			Vector2 p = convexVertices[ i ];
			vertices[ i ] = Plane2World( p.x, p.y, plane );
			colors[ i ] = color;
			uv[ i ] = p;
			uv2[ i ] = p;

			if ( i > 1 )
			{
				int t = ( i - 2 ) * 3;
				triangles[ t + 0 ] = 0;
				triangles[ t + 1 ] = flipNormals ? i : i - 1;
				triangles[ t + 2 ] = flipNormals ? i - 1 : i;
			}
		}

		mesh.vertices = vertices;
		mesh.colors = colors;
		mesh.triangles = triangles;
		mesh.uv = uv;

		mesh.RecalculateNormals();
		mesh.RecalculateBounds();

		Vector2 mins = World2Plane( mesh.bounds.min, plane);
		Vector2 maxs = World2Plane( mesh.bounds.max, plane);

		float padding = 0.001f;
		for ( int i = 0; i < convexVertices.Length; i++ )
		{
			uv2[ i ].x = ( uv2[ i ].x - mins.x ) / ( maxs.x - mins.x );
			uv2[ i ].y = ( uv2[ i ].y - mins.y ) / ( maxs.y - mins.y );

			uv2[ i ].x = uv2[ i ].x * ( 1f - 2f * padding ) + padding;
			uv2[ i ].y = uv2[ i ].y * ( 1f - 2f * padding ) + padding;
		}

		mesh.uv2 = uv2;

		return mesh;
	}
	
	/// <summary>
	/// ������� ��� �����
	/// </summary>
	/// <param name="center">�������� ������ � ��������� �����������</param>
	/// <param name="radius">������ �����</param>
	/// <param name="segments">����� ���������/������</param>
	/// <param name="color">���� ������</param>
	/// <param name="plane">���������</param>
	/// <returns></returns>
	public static Mesh CreateSolidCircle( Vector3 center, float radius, int segments, Color color, AxisPlane plane )
	{
		Mesh mesh = new Mesh();

		int numVertices = segments + 1;
		float angleDelta = Mathf.PI * 2f / (float) segments;
		
		Vector3[] vertices = new Vector3[ numVertices ];
		Color[] colors = new Color[ numVertices ];
		int[] triangles = new int[ segments * 3 ];
		
		colors[ 0 ] = color;
		
		for ( int v = 1, t = 0; v < numVertices; v++, t += 3 )
		{
			float angle = angleDelta * (float) ( v - 1 );

			vertices[ v ] = CreatePolarVertex( center, radius, angle, plane );
			
			triangles[ t + 0 ] = 0;
			triangles[ t + 1 ] = ( v == vertices.Length - 1 ) ? 1 : v + 1;
			triangles[ t + 2 ] = v;
			
			colors[ v ] = color;
		}
		
		mesh.vertices = vertices;
		mesh.colors = colors;
		mesh.triangles = triangles;
		//meshObject.uv = uv;
		
		return mesh;
	}

	/// <summary>
	/// ������������� � ������������� ������
	/// </summary>
	/// <param name="center"></param>
	/// <param name="radius"></param>
	/// <param name="roundSegments"></param>
	/// <param name="color"></param>
	/// <param name="plane"></param>
	/// <returns></returns>
	public static Mesh CreateSolidRoundedRect( float width, float height, float roundRadius, int roundSegments, Color color, AxisPlane plane )
	{
		Mesh mesh = new Mesh();

		int roundVertices = roundSegments + 1;
		int totalSegments = roundVertices * 4;
		int numVertices = totalSegments + 1; // ����� ������ = ����� ��������� + ����������� �������
		float angleDelta = MathUtils.HALF_PI / ( float ) roundSegments;

		Vector3[] vertices = new Vector3[ numVertices ];
		Color[] colors = new Color[ numVertices ];
		int[] triangles = new int[ totalSegments * 3 ];

		float cx = width * 0.5f - roundRadius;
		float cy = height * 0.5f - roundRadius;

		colors[ 0 ] = color;

		int v = 1;
		int t = 0;

		float angle = MathUtils.HALF_PI;

		bool flip = false;

		for ( int corner = 0; corner < 4; corner++ )
		{
			float sin = Mathf.Sin( angle );
			float cos = Mathf.Cos( angle );

			float x = cos * cy - sin * cx;
			float y = cos * cx + sin * cy;

			if ( flip )
			{
				x = cos * cx - sin * cy;
				y = cos * cy + sin * cx;
			}

			Vector3 c = Plane2World( x, y, plane );

			for ( int i = 0; i < roundVertices; i++ )
			{
				vertices[ v ] = CreatePolarVertex( c, roundRadius, angle, plane );
				colors[ v ] = color;
				
				triangles[ t++ ] = 0;
				triangles[ t++ ] = ( v == vertices.Length - 1 ) ? 1 : v + 1;
				triangles[ t++ ] = v;

				if ( i < roundVertices - 1 )
					angle += angleDelta;

				v++;
			}

			flip = !flip;
		}

		mesh.vertices = vertices;
		mesh.colors = colors;
		mesh.triangles = triangles;

		return mesh;
	}

	/// <summary>
	/// ������� ��� ����������
	/// </summary>
	/// <param name="center">�������� ������ � ��������� �����������</param>
	/// <param name="radius">������ ����������</param>
	/// <param name="lineWidth">������� ����� ����������</param>
	/// <param name="segments">����� ��������� ����������</param>
	/// <param name="color">���� ������</param>
	/// <param name="plane">���������, � ������� ���������� ���</param>
	/// <returns></returns>
	public static Mesh CreateWireCircle( Vector3 center, float radius, float lineWidth, int segments, Color color, AxisPlane plane )
	{
		Mesh mesh = new Mesh();

		int numVertices = segments * 2;

		Vector3[] vertices = new Vector3[ numVertices ];
		Color[] colors = new Color[ numVertices ];
		int[] triangles = new int[ segments * 6 ];

		float r0 = radius - lineWidth * 0.5f;
		float r1 = r0 + lineWidth;
		float angleDelta = Mathf.PI * 2f / (float) segments;

		int vCurrIndex = 0;
		int tCurrIndex = 0;

		int vPrevIndex0 = ( segments - 1 ) * 2;
		int vPrevIndex1 = vPrevIndex0 + 1;

		float angle = 0f;

		CreatePolarArc( vertices, colors, triangles, ref vCurrIndex, ref tCurrIndex, ref vPrevIndex0, ref vPrevIndex1, ref angle,
			center, angleDelta, segments, plane, r0, r1, color, color );

		mesh.vertices = vertices;
		mesh.colors = colors;
		mesh.triangles = triangles;

		return mesh;
	}

	/// <summary>
	/// ������� ��� ����
	/// </summary>
	/// <param name="center">�������� ������ � ��������� �����������</param>
	/// <param name="radius">������ �������� ����</param>
	/// <param name="minAngle">��������� ����� ����</param>
	/// <param name="arcAngle">���� ����</param>
	/// <param name="lineWidth">������� ����� ����</param>
	/// <param name="majorSegments">����� ��������� ����</param>
	/// <param name="minorSegments">����� ��������� ���������� ���� �� ������</param>
	/// <param name="color">���� ������</param>
	/// <param name="plane">���������, � ������� ���������� ���</param>
	/// <returns></returns>
	public static Mesh CreateArc( Vector3 center, float radius, float minAngle, float arcAngle, float lineWidth, int majorSegments, int minorSegments, Color color, AxisPlane plane )
	{
		Mesh mesh = new Mesh();

		if ( minorSegments < 1 )
			minorSegments = 1;

		int numVertices = ( majorSegments + minorSegments ) * 2;
		int numIndices = ( majorSegments + minorSegments - 1 ) * 6;

		Vector3[] vertices = new Vector3[ numVertices ];
		Color[] colors = new Color[ numVertices ];
		int[] triangles = new int[ numIndices ];

		float r0 = radius - lineWidth * 0.5f;
		float r1 = r0 + lineWidth;

		float angleDelta = arcAngle / (float) majorSegments;

		int vCurrIndex = 0;
		int tCurrIndex = 0;

		int vPrevIndex0 = 0;
		int vPrevIndex1 = 0;

		float angle = minAngle;

		// MAJOR ARC

		CreatePolarArc( vertices, colors, triangles,
			ref vCurrIndex, ref tCurrIndex, ref vPrevIndex0, ref vPrevIndex1, ref angle,
			center, angleDelta, majorSegments, plane, r0, r1, color, color, true );

		//for ( int i = 0; i < majorSegments; i++ )
		//{
		//    float angle = minAngle + angleDelta * (float) ( i + 1 );

		//    CreatePolarSegment( vertices, colors, triangles,
		//        ref vCurrIndex, ref tCurrIndex, ref vPrevIndex0, ref vPrevIndex1,
		//        center, angle, plane, r0, r1, color, color );
		//}

		// ROUNDING HALF-CIRCLES OF ARC'S ENDS

		if ( minorSegments > 1 )
		{
			float maxAngle = minAngle + arcAngle;
			if ( minAngle > maxAngle )
				MathUtils.Swap( ref minAngle, ref maxAngle );

			float currentMinAngle = minAngle - Mathf.PI;
			float currentMaxAngle = maxAngle;

			float minorRadius = lineWidth * 0.5f;

			Vector3 minAngleCenter = CreatePolarVertex( center, radius, minAngle, plane );
			Vector3 maxAngleCenter = CreatePolarVertex( center, radius, maxAngle, plane );

			angleDelta = Mathf.PI / (float) minorSegments;

			int vPrevMinIndex = 0;
			int vPrevMaxIndex = vPrevIndex1;
			
			int vLastMinIndex = 1;
			int vLastMaxIndex = vPrevIndex0;

			for ( int i = 0; i < minorSegments - 1; i++ )
			{
				currentMinAngle += angleDelta;
				currentMaxAngle += angleDelta;

				int vMinIndex = vCurrIndex++;
				int vMaxIndex = vCurrIndex++;

				vertices[ vMinIndex ] = CreatePolarVertex( minAngleCenter, minorRadius, currentMinAngle, plane );
				vertices[ vMaxIndex ] = CreatePolarVertex( maxAngleCenter, minorRadius, currentMaxAngle, plane );

				colors[ vMinIndex ] = color;
				colors[ vMaxIndex ] = color;

				triangles[ tCurrIndex++ ] = vPrevMinIndex;
				triangles[ tCurrIndex++ ] = vLastMinIndex;
				triangles[ tCurrIndex++ ] = vMinIndex;

				triangles[ tCurrIndex++ ] = vPrevMaxIndex;
				triangles[ tCurrIndex++ ] = vLastMaxIndex;
				triangles[ tCurrIndex++ ] = vMaxIndex;

				vPrevMinIndex = vMinIndex;
				vPrevMaxIndex = vMaxIndex;
			}
		}

		mesh.vertices = vertices;
		mesh.colors = colors;
		mesh.triangles = triangles;

		return mesh;
	}

	/// <summary>
	/// ������� glow-fx ��� ��� ������������ �����
	/// </summary>
	/// <param name="center">�������� ������</param>
	/// <param name="circleRadius">������ �����</param>
	/// <param name="glowRadius">������ ��������</param>
	/// <param name="segments">���-�� ��������� �� �����</param>
	/// <param name="color">����</param>
	/// <param name="plane">���������</param>
	/// <returns></returns>
	public static Mesh CreateSolidCircleGlow( Vector3 center, float circleRadius, float glowRadius, int segments, Color color, AxisPlane plane )
	{
		Mesh mesh = new Mesh();

		int numVertices = segments * 2;

		Vector3[] vertices = new Vector3[ numVertices ];
		Color[] colors = new Color[ numVertices ];
		int[] triangles = new int[ numVertices * 3 ];

		float r0 = circleRadius;
		float r1 = r0 + glowRadius;
		float angleDelta = Mathf.PI * 2f / (float) segments;

		int vCurrIndex = 0;
		int tCurrIndex = 0;

		int vPrevIndex0 = ( segments - 1 ) * 2;
		int vPrevIndex1 = vPrevIndex0 + 1;

		float angle = 0f;

		Color color1 = new Color( color.r, color.g, color.b, 0f );

		CreatePolarArc( vertices, colors, triangles, ref vCurrIndex, ref tCurrIndex, ref vPrevIndex0, ref vPrevIndex1, ref angle,
			center, angleDelta, segments, plane, r0, r1, color, color1 );

		mesh.vertices = vertices;
		mesh.colors = colors;
		mesh.triangles = triangles;

		return mesh;
	}

	/// <summary>
	/// ������� glow-��� ��� ����������
	/// </summary>
	/// <param name="center">�������� ������</param>
	/// <param name="circleRadius">������ �����</param>
	/// <param name="lineWidth">������� ����� ����������</param>
	/// <param name="glowRadius">������ ��������</param>
	/// <param name="segments">���-�� ��������� �� �����</param>
	/// <param name="color">����</param>
	/// <param name="plane">���������</param>
	/// <returns></returns>
	public static Mesh CreateWireCircleGlow( Vector3 center, float circleRadius, float lineWidth, float glowRadius, int segments, Color color, AxisPlane plane )
	{
		Mesh mesh = new Mesh();

		int numVertices = segments * 4;

		Vector3[] vertices = new Vector3[ numVertices ];
		Color[] colors = new Color[ numVertices ];
		int[] triangles = new int[ numVertices * 3 ];

		float angleDelta = Mathf.PI * 2f / (float) segments;

		Color color1 = new Color( color.r, color.g, color.b, 0f );

		int vCurrIndex = 0;
		int tCurrIndex = 0;

		// Inner Arc

		float innerRadius0 = circleRadius - lineWidth * 0.5f;
		float innerRadius1 = innerRadius0 - glowRadius;
		
		int vPrevIndex0 = ( segments - 1 ) * 2;
		int vPrevIndex1 = vPrevIndex0 + 1;

		float angle = 0f;

		CreatePolarArc( vertices, colors, triangles,
			ref vCurrIndex, ref tCurrIndex, ref vPrevIndex0, ref vPrevIndex1, ref angle,
			center, angleDelta, segments, plane, innerRadius0, innerRadius1, color, color1 );

		// Outer Arc

		float outerRadius0 = circleRadius + lineWidth * 0.5f;
		float outerRadius1 = outerRadius0 + glowRadius;
		
		vPrevIndex0 = ( segments * 2 - 1 ) * 2;
		vPrevIndex1 = vPrevIndex0 + 1;
		
		angle = 0f;

		CreatePolarArc( vertices, colors, triangles,
			ref vCurrIndex, ref tCurrIndex, ref vPrevIndex0, ref vPrevIndex1, ref angle,
			center, angleDelta, segments, plane, outerRadius0, outerRadius1, color, color1 );

		mesh.vertices = vertices;
		mesh.colors = colors;
		mesh.triangles = triangles;

		return mesh;
	}

	/// <summary>
	/// ������� glow-��� ����
	/// </summary>
	/// <param name="center">�������� ������ � ��������� �����������</param>
	/// <param name="radius">������ �������� ����</param>
	/// <param name="minAngle">��������� ����� ����</param>
	/// <param name="arcAngle">���� ����</param>
	/// <param name="lineWidth">������� ����� ����</param>
	/// <param name="majorSegments">����� ��������� ����</param>
	/// <param name="minorSegments">����� ��������� ���������� ���� �� ������</param>
	/// <param name="glowRadius">������ ��������</param>
	/// <param name="color">���� ������</param>
	/// <param name="plane">���������, � ������� ���������� ���</param>
	/// <returns></returns>
	public static Mesh CreateArcGlow( Vector3 center, float radius, float minAngle, float arcAngle, float lineWidth, int majorSegments, int minorSegments, float glowRadius, Color color, AxisPlane plane )
	{
		Mesh mesh = new Mesh();

		if ( minorSegments < 1 )
			minorSegments = 1;

		int numVertices = ( majorSegments + minorSegments ) * 4;

		Vector3[] vertices = new Vector3[ numVertices ];
		Color[] colors = new Color[ numVertices ];
		int[] triangles = new int[ numVertices * 3 ];

		float majorAngleDelta = arcAngle / (float) majorSegments;
		float minorAngleDelta = Mathf.PI / (float) minorSegments;

		Color color1 = new Color( color.r, color.g, color.b, 0f );

		float innerRadius0 = radius - lineWidth * 0.5f;
		float innerRadius1 = innerRadius0 - glowRadius;
		float outerRadius0 = radius + lineWidth * 0.5f;
		float outerRadius1 = outerRadius0 + glowRadius;
		float minorRadius0 = lineWidth * 0.5f;
		float minorRadius1 = minorRadius0 + glowRadius;

		int vCurrIndex = 0;
		int tCurrIndex = 0;

		int vPrevIndex0 = numVertices - 2;
		int vPrevIndex1 = vPrevIndex0 + 1;

		// INNER MAJOR ARC
		
		float angle = minAngle;

		CreatePolarArc( vertices, colors, triangles,
			ref vCurrIndex, ref tCurrIndex, ref vPrevIndex0, ref vPrevIndex1, ref angle,
			center, majorAngleDelta, majorSegments, plane, innerRadius0, innerRadius1, color, color1 );
		
		// FIRST MINOR ARC

		Vector3 minorCenter = CreatePolarVertex( center, radius, angle, plane );
		angle += Mathf.PI;

		CreatePolarArc( vertices, colors, triangles,
			ref vCurrIndex, ref tCurrIndex, ref vPrevIndex0, ref vPrevIndex1, ref angle,
		    minorCenter, -minorAngleDelta, minorSegments, plane, minorRadius0, minorRadius1, color, color1 );

		// OUTER MAJOR ARC

		CreatePolarArc( vertices, colors, triangles,
		    ref vCurrIndex, ref tCurrIndex, ref vPrevIndex0, ref vPrevIndex1, ref angle,
		    center, -majorAngleDelta, majorSegments, plane, outerRadius0, outerRadius1, color, color1 );

		// SECOND MINOR ARC

		minorCenter = CreatePolarVertex( center, radius, angle, plane );
		//angle -= Mathf.PI;

		CreatePolarArc( vertices, colors, triangles,
			ref vCurrIndex, ref tCurrIndex, ref vPrevIndex0, ref vPrevIndex1, ref angle,
		    minorCenter, -minorAngleDelta, minorSegments, plane, minorRadius0, minorRadius1, color, color1 );


		//// MAJOR ARC

		//for ( int i = 0; i < majorSegments + 1; i++ )
		//{
		//    float angle = minAngle + angleDelta * (float) i;

		//    bool ignoreTriangles = false;
		//    if ( i == 0 )
		//    {
		//        ignoreTriangles = true;
		//    }

		//    CreatePolarSegment( vertices, colors, triangles,
		//        ref vCurrIndex, ref tCurrIndex, ref vLastInnerIndex0, ref vLastInnerIndex1,
		//        center, angle, plane, innerRadius0, innerRadius1, color, color, ignoreTriangles );

		//    CreatePolarSegment( vertices, colors, triangles,
		//        ref vCurrIndex, ref tCurrIndex, ref vLastOuterIndex0, ref vLastOuterIndex1,
		//        center, angle, plane, outerRadius0, outerRadius1, color, color, ignoreTriangles );


		//}

		//// ROUNDING HALF-CIRCLES OF ARC'S ENDS

		//if ( minorSegments > 1 )
		//{
		//    float maxAngle = minAngle + arcAngle;
		//    if ( minAngle > maxAngle )
		//        MathUtils.Swap( ref minAngle, ref maxAngle );

		//    float currentMinAngle = minAngle - Mathf.PI;
		//    float currentMaxAngle = maxAngle;

		//    float minorRadius = lineWidth * 0.5f;

		//    Vector3 minAngleCenter = CreatePolarVertex( center, radius, minAngle, plane );
		//    Vector3 maxAngleCenter = CreatePolarVertex( center, radius, maxAngle, plane );

		//    angleDelta = Mathf.PI / (float) minorSegments;

		//    int vPrevMinIndex = 0;
		//    int vPrevMaxIndex = vLastIndex1;

		//    int vLastMinIndex = 1;
		//    int vLastMaxIndex = vLastIndex0;

		//    for ( int i = 0; i < minorSegments - 1; i++ )
		//    {
		//        currentMinAngle += angleDelta;
		//        currentMaxAngle += angleDelta;

		//        int vMinIndex = vCurrIndex++;
		//        int vMaxIndex = vCurrIndex++;

		//        vertices[ vMinIndex ] = CreatePolarVertex( minAngleCenter, minorRadius, currentMinAngle, plane );
		//        vertices[ vMaxIndex ] = CreatePolarVertex( maxAngleCenter, minorRadius, currentMaxAngle, plane );

		//        colors[ vMinIndex ] = color;
		//        colors[ vMaxIndex ] = color;

		//        triangles[ tCurrIndex++ ] = vPrevMinIndex;
		//        triangles[ tCurrIndex++ ] = vLastMinIndex;
		//        triangles[ tCurrIndex++ ] = vMinIndex;

		//        triangles[ tCurrIndex++ ] = vPrevMaxIndex;
		//        triangles[ tCurrIndex++ ] = vLastMaxIndex;
		//        triangles[ tCurrIndex++ ] = vMaxIndex;

		//        vPrevMinIndex = vMinIndex;
		//        vPrevMaxIndex = vMaxIndex;
		//    }
		//}

		mesh.vertices = vertices;
		mesh.colors = colors;
		mesh.triangles = triangles;

		return mesh;
	}

	/// <summary>
	/// ������� ������� ����� �������� ����������
	/// </summary>
	/// <param name="center">�����</param>
	/// <param name="radius">����������</param>
	/// <param name="angle">����</param>
	/// <param name="plane">������� ��</param>
	/// <returns></returns>
	public static Vector3 CreatePolarVertex( Vector3 center, float radius, float angle, AxisPlane plane )
	{
		float x = Mathf.Cos( angle ) * radius;
		float y = Mathf.Sin( angle ) * radius;

		Vector3 v = Vector3.zero;

		switch ( plane )
		{
			case AxisPlane.ZY:
				v.z = x;
				v.y = y;
				break;

			case AxisPlane.XZ:
				v.x = x;
				v.z = y;
				break;

			case AxisPlane.XY:
				v.x = x;
				v.y = y;
				break;
		}

		return center + v;
	}

	/// <summary>
	/// ��������� ���������� �� ������� � �������, ����������� �� ��������� ���������
	/// </summary>
	/// <param name="x"></param>
	/// <param name="y"></param>
	/// <param name="plane"></param>
	/// <returns></returns>
	public static Vector3 Plane2World( float x, float y, AxisPlane plane )
	{
		switch ( plane )
		{
			case AxisPlane.ZY: return new Vector3( 0f, y, x );
			case AxisPlane.XZ: return new Vector3( x, 0f, y );
			case AxisPlane.XY: return new Vector3( x, y, 0f );
		}

		throw new UnityException("Unknown plane value");
	}

	public static Vector2 World2Plane( Vector3 w, AxisPlane plane )
	{
		switch ( plane )
		{
			case AxisPlane.ZY: return new Vector2( w.z, w.y );
			case AxisPlane.XZ: return new Vector2( w.x, w.z );
			case AxisPlane.XY: return new Vector2( w.x, w.y );
		}

		throw new UnityException( "Unknown plane value" );
	}

	public static void CreatePolarSegment(
		Vector3[] vertices, Color[] colors, int[] triangles,
		ref int vCurrIndex, ref int tCurrIndex, ref int vPrevIndex0, ref int vPrevIndex1,
		Vector3 center, float angle, AxisPlane plane,
		float radius0, float radius1,
		Color color0, Color color1,
		bool ignoreTriangles = false, bool ignoreVertices = false, bool flipSide = false )
	{
		int vIndex0 = vCurrIndex++;
		int vIndex1 = vCurrIndex++;

		if ( !ignoreVertices )
		{
			vertices[ vIndex0 ] = CreatePolarVertex( center, radius0, angle, plane );
			vertices[ vIndex1 ] = CreatePolarVertex( center, radius1, angle, plane );

			colors[ vIndex0 ] = color0;
			colors[ vIndex1 ] = color1;
		}

		if ( !ignoreTriangles )
		{
			if ( radius0 > radius1 )
				flipSide = !flipSide;

			if ( flipSide )
			{
				triangles[ tCurrIndex++ ] = vIndex1;
				triangles[ tCurrIndex++ ] = vIndex0;
				triangles[ tCurrIndex++ ] = vPrevIndex1;

				triangles[ tCurrIndex++ ] = vPrevIndex0;
				triangles[ tCurrIndex++ ] = vPrevIndex1;
				triangles[ tCurrIndex++ ] = vIndex0;
			}
			else
			{
				triangles[ tCurrIndex++ ] = vIndex0;
				triangles[ tCurrIndex++ ] = vIndex1;
				triangles[ tCurrIndex++ ] = vPrevIndex1;

				triangles[ tCurrIndex++ ] = vPrevIndex1;
				triangles[ tCurrIndex++ ] = vPrevIndex0;
				triangles[ tCurrIndex++ ] = vIndex0;
			}
		}

		vPrevIndex0 = vIndex0;
		vPrevIndex1 = vIndex1;
	}

	public static void CreatePolarArc(
		Vector3[] vertices, Color[] colors, int[] triangles,
		ref int vCurrIndex, ref int tCurrIndex, ref int vPrevIndex0, ref int vPrevIndex1, ref float currAngle,
		Vector3 center, float deltaAngle, int segments, AxisPlane plane,
		float radius0, float radius1, Color color0, Color color1, bool createFirstSegment = false )
	{
		bool flipSide = false;
		if ( deltaAngle < 0f )
			flipSide = !flipSide;

		if ( createFirstSegment )
		{
			CreatePolarSegment( vertices, colors, triangles,
				ref vCurrIndex, ref tCurrIndex, ref vPrevIndex0, ref vPrevIndex1,
				center, currAngle, plane, radius0, radius1, color0, color1, true, false, flipSide );
		}

		for ( int i = 0; i < segments; i++ )
		{
			currAngle += deltaAngle;

			CreatePolarSegment( vertices, colors, triangles,
				ref vCurrIndex, ref tCurrIndex, ref vPrevIndex0, ref vPrevIndex1,
				center, currAngle, plane, radius0, radius1, color0, color1, false, false, flipSide );
		}
	}
}
