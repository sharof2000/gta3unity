﻿using UnityEngine;
using System.Collections;

namespace GTA3
{
	public static class COL
	{
		public const int MODEL_NAME_SIZE = 22;
	}

	public class COL_Model
	{
		// HEADER
		public string fourCC;			// FourCC ("COLL", "COL2" or "COL3")
		public int size;				// file size from after this value (so 8 byte less)
		public string name;	// collision model name
		public short gameModelIndex;	// game model index (for acceleration, not a must)
		public COL_Bounds bounds;		// bounding objects, see above

		// DATA
		public COL_Sphere[] spheres;
		public COL_Box[] boxes;
		public COL_Vertex[] vertices;
		public COL_Face[] faces;
	}

	public struct COL_Bounds
	{
		public float radius;
		public Vector3 center;
		public Vector3 min;
		public Vector3 max;

		public override string ToString()
		{
			return "COL_Bounds[ radius: " + radius + ", center: " + center + ", min: " + min + ", max: " + max + " ]";
		}
	}

	public struct COL_Surface
	{
		public byte material;
		public byte flag;
		public byte unknown;
		public byte light;

		public override string ToString()
		{
			return "COL_Surface[ material: " + material + ", flag: " + flag + ", unknown: " + unknown + ", light: " + light + " ]";
		}
	}

	public struct COL_Sphere
	{
		public float radius;
		public Vector3 center;
		public COL_Surface surface;

		public override string ToString()
		{
			return "COL_Sphere[ radius: " + radius + ", center: " + center + ", surface: " + surface + " ]";
		}
	}

	public struct COL_Box
	{
		public Vector3 min;
		public Vector3 max;
		public COL_Surface surface;

		public override string ToString()
		{
			return "COL_Box[ min: " + min + ", max: " + max + ", surface: " + surface + " ]";
		}
	}

	public struct COL_Vertex
	{
		public Vector3 v;

		public override string ToString()
		{
			return "COL_Vertex[ " + v + " ]";
		}
	}

	public struct COL_Face
	{
		public int v1;
		public int v2;
		public int v3;
		public COL_Surface surface;

		public override string ToString()
		{
			return "COL_Face[ " + v1 + ", " + v2 + ", " + v3 + " ]";
		}
	}
}