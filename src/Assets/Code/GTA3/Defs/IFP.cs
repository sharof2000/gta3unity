﻿using UnityEngine;
using System.Collections;

namespace GTA3
{
	// BASE - Section Base Header
	public struct IFP_Base
	{
		public string id;
		public int offset;
		public int seekpos;
	}
	// TAnimation
	public class IFP_Animation
	{
		public string name;
		public IFP_DGAN data;
	}

	// DGAN - Holds data for IFP_Animation
	public class IFP_DGAN
	{
		public IFP_CPAN[] info;
	}

	// CPAN - Holds data for animation objects
	public class IFP_CPAN
	{
		public IFP_ANIM anim;
		public IFP_Keyframe[] keyframes;
		public string keyframeType;
	}

	// ANIM - Holds data for animation frames
	public class IFP_ANIM
	{
		public string objectName;
		public int fakeFrameCount;
		public int unknown;
		public int nextSibling;
		public int prevSibling;
	}

	// KFRM - 'Abstract' section inherting from BASE and representing a frame type.
	public class IFP_Keyframe
	{
		public float time = 0f;
		public Quaternion rot = Quaternion.identity;
		public Vector3 pos = Vector3.zero;
		public Vector3 scale = Vector3.one;
	}
}