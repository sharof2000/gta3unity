﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.IO;
using System.Linq;

namespace GTA3
{
	public enum ImporterMode
	{
		// BUILDING BLOCKS
		Models,
		Textures,
		Collisions,
		Animations,
		Items,
		Cars,
		Map,

		// LOT OF WORK
		//Peds,
		//Wheels,
		//Cars
	}

	public class ImporterWindow : EditorWindow
	{
		public const string NONE = "[NONE]";
		public const string TITLE = "GTA3 Importer";

		Importer importer = new Importer();

		private ImporterMode mode;
		//private string[] mappaths = {};
		//private string[] mapnames = {};
		private bool pathsOnly = false;

		private string dffname = "weapons";
		private string txdname = "misc";
		private string ifpname = "ped";

		//private string[] collisionPaths = {};
		//private string[] collisionNames = {};

		// Selected
		private int mapIndex = 0;
		private int ideIndex = 0;
		private int colIndex = 0;
		private int carIndex = 0;
		private int handlingIndex = 0;

		private int itemId = 2170;

		private Transform hierarchy = null;
		private WrapMode animWrapMode = WrapMode.Loop;

		//private AnimationClip clip;

		[MenuItem( "Window/" + TITLE )]
		private static void ShowWindow()
		{
			ImporterWindow editor = GetWindow( typeof( ImporterWindow ), false, TITLE ) as ImporterWindow;
			editor.UpdateDataLists();
		}

		private void UpdateDataLists()
		{
			importer.UpdateSrcDataPaths();
			importer.UpdateCarDefs();

			mapIndex = Mathf.Clamp( mapIndex, 0, importer.iplPaths.Length - 1 );
			colIndex = Mathf.Clamp( colIndex, 0, importer.colPaths.Length - 1 );
			carIndex = Mathf.Clamp( carIndex, 0, importer.carDefs.Length - 1 );
		}

		void OnGUI()
		{
			// Предотвращаем зависание прогресс бара в случае ексепшена
			try
			{
				ProcessGUI();
			}
			catch ( UnityException ex )
			{
				EditorUtility.ClearProgressBar();
				throw ex;
			}
			catch ( System.SystemException ex )
			{
				EditorUtility.ClearProgressBar();
				throw ex;
			}
		}

		private void ProcessGUI()
		{
			EditorGUILayout.BeginVertical();

			//if ( GUILayout.Button( "Test" ) )
			//{
			//	Debug.Log( GetAssetPath( ItemType.Unknown, "wheels", AssetType.Material, "sommat" ) );
			//	Debug.Log( GetAssetPath( ItemType.Wheel, "wheels", AssetType.Prefab, "jk" ) );
			//}

			//----------------------------------------------------------
			// Параметры
			//----------------------------------------------------------

			EditorGUILayout.Space();
			mode = ( ImporterMode ) EditorGUILayout.EnumPopup( "Importer Mode", mode );

			if ( GUILayout.Button( "Update Data File List" ) )
			{
				UpdateDataLists();
			}

			EditorGUILayout.Space();
			EditorGUILayout.LabelField( "Shared Options", EditorStyles.boldLabel );

			switch ( mode )
			{
				case ImporterMode.Models:
				case ImporterMode.Collisions:
				case ImporterMode.Items:
				case ImporterMode.Cars:
					importer.scale = EditorGUILayout.FloatField( "Scale", importer.scale );
					break;
			}

			switch ( mode )
			{
				case ImporterMode.Models:
				case ImporterMode.Textures:
				case ImporterMode.Items:
				case ImporterMode.Cars:
					importer.textureFilter = ( FilterMode ) EditorGUILayout.EnumPopup( "Texture Filter", importer.textureFilter );
					break;
			}

			switch ( mode )
			{
				case ImporterMode.Models:
				case ImporterMode.Items:
				case ImporterMode.Cars:
					EditorGUILayout.Space();
					importer.createModelPrefabs = EditorGUILayout.Toggle( "Create Model Prefabs", importer.createModelPrefabs );
					break;
			}

			switch ( mode )
			{
				case ImporterMode.Items:
				case ImporterMode.Cars:
					importer.createItemPrefabs = EditorGUILayout.Toggle( "Create Item Prefabs", importer.createItemPrefabs );
					break;
			}

			switch ( mode )
			{
				case ImporterMode.Models:
				case ImporterMode.Items:
				case ImporterMode.Cars:
					EditorGUILayout.Space();
					importer.createMeshAssets = EditorGUILayout.Toggle( "Create Mesh Assets", importer.createMeshAssets );
					importer.createTextureAssets = EditorGUILayout.Toggle( "Create Texture Assets", importer.createTextureAssets );
					importer.createMaterialAssets = EditorGUILayout.Toggle( "Create Material Assets", importer.createMaterialAssets );
					break;
			}

			switch ( mode )
			{
				case ImporterMode.Collisions:
				case ImporterMode.Items:
				case ImporterMode.Cars:
					EditorGUILayout.Space();
					importer.createSphereColliders = EditorGUILayout.Toggle( "Create Sphere Colliders", importer.createSphereColliders );
					importer.createBoxColliders = EditorGUILayout.Toggle( "Create Box Colliders", importer.createBoxColliders );
					importer.createMeshColliders = EditorGUILayout.Toggle( "Create Mesh Colliders", importer.createMeshColliders );
					importer.customMeshColliders = ( Transform ) EditorGUILayout.ObjectField( "Custom Mesh Colliders", importer.customMeshColliders, typeof( Transform ) );
					break;
			}

			switch ( mode )
			{
				case ImporterMode.Models:

					EditorGUILayout.Space();
					EditorGUILayout.LabelField( "Model", EditorStyles.boldLabel );

					dffname = EditorGUILayout.TextField( "DFF name", dffname );
					txdname = EditorGUILayout.TextField( "TXD name", txdname );

					if ( GUILayout.Button( "Import Model (DFF)" ) )
					{
						Frame obj = importer.ImportModel( dffname, false, false );
						importer.Clear();
					}

					if ( GUILayout.Button( "Import Model with Texture (DFF, TXD)" ) )
					{
						importer.ImportTXD( txdname );
						importer.ImportModel( dffname, true, false );
						importer.Clear();
					}

					if ( GUILayout.Button( "Import Generic Models with Textures (DFF, TXD) " ) )
					{
						importer.ImportGenerics( false );
						importer.Clear();
					}

					break;

				case ImporterMode.Textures:

					EditorGUILayout.Space();
					EditorGUILayout.LabelField( "Textures", EditorStyles.boldLabel );

					txdname = EditorGUILayout.TextField( "TXD name", txdname );

					if ( GUILayout.Button( "Import Textures from TXD" ) )
					{
						importer.ImportTXD( txdname );
						importer.Clear();
					}

					if ( GUILayout.Button( "Import All Textures (*.txd)" ) )
					{
						string[] paths = AssetUtils.FindFilesByFilter( Importer.PATH_TEXTURES, "*.txd" );

						Importer.Progress_Start( "Importing Textures (*.txd)" );
						for ( int i = 0; i < paths.Length; i++ )
						{
							bool cancel = Importer.Progress_Update( "txd imported: " + i + " / " + paths.Length, ( float ) i / ( float ) paths.Length );
							if ( cancel )
								break;

							//if ( makeSubDirs )
							//{
							//	string txdsubdir = Path.GetFileNameWithoutExtension( paths[ i ] );
							//	txdsubdir = GetTextureDirPath( txdsubdir );
							//	if ( Directory.Exists( txdsubdir ) )
							//		continue;
							//}

							importer.ImportTxdAtPath( paths[ i ] );
						}
						importer.Clear();
						Importer.Progress_Stop();
					}

					break;

				case ImporterMode.Collisions:
					
					EditorGUILayout.Space();
					EditorGUILayout.LabelField( "Collisions", EditorStyles.boldLabel );

					colIndex = EditorGUILayout.Popup( "Collision File", colIndex, importer.colNames );

					if ( GUILayout.Button( "Import Collisions (.col)" ) )
					{
						if ( colIndex >= 0 && colIndex < importer.colPaths.Length )
						{
							importer.ImportCollisionsAtPath( importer.colPaths[ colIndex ] );
							importer.Clear();
						}
					}

					break;

				case ImporterMode.Animations:

					// Animations

					EditorGUILayout.Space();
					EditorGUILayout.LabelField( "Animations", EditorStyles.boldLabel );

					ifpname = EditorGUILayout.TextField( "IFP name", ifpname );
					hierarchy = ( Transform ) EditorGUILayout.ObjectField( "Hierarchy", hierarchy, typeof( Transform ) );
					animWrapMode = ( WrapMode ) EditorGUILayout.EnumPopup( "Clips Wrap Mode", animWrapMode );

					EditorGUILayout.Space();
					importer.animUseKeyframeRotation = EditorGUILayout.Toggle( "Use Keyframe Rotation", importer.animUseKeyframeRotation );
					importer.animUseKeyframePosition = EditorGUILayout.Toggle( "Use Keyframe Position", importer.animUseKeyframePosition );
					importer.animUseKeyframeScale = EditorGUILayout.Toggle( "Use Keyframe Scale", importer.animUseKeyframeScale );

					EditorGUILayout.Space();
					if ( hierarchy )
					{
						if ( GUILayout.Button( "Import Animations (IFP)" ) )
						{
							importer.ImportAnimation( ifpname, hierarchy, animWrapMode );
							importer.Clear();
						}
					}
					else
					{
						EditorGUILayout.HelpBox( "Необходимо задать объект иерархии (Hierarchy). Обычно это модель игрока.", MessageType.Error );
					}

					break;

				case ImporterMode.Items:

					// ITEMS

					EditorGUILayout.Space();
					EditorGUILayout.LabelField( "Item", EditorStyles.boldLabel );
					EditorGUILayout.HelpBox( "Типы импортируемых файлов:\nIDE, DFF, TXD, COL", MessageType.Info );

					itemId = EditorGUILayout.IntField( "Item ID", itemId );
					ideIndex = EditorGUILayout.Popup( "IDE File", ideIndex, importer.ideNames );
					colIndex = EditorGUILayout.Popup( "Collision File", colIndex, importer.colNames );

					if ( GUILayout.Button( "Import Item" ) )
					{
						string colname = importer.colNames[ colIndex ];
						string idename = importer.ideNames[ ideIndex ];
						importer.ImportItem( itemId, false, colname, idename );
						importer.Clear();
					}

					//EditorGUILayout.Space();
					//EditorGUILayout.LabelField( "Item Groups", EditorStyles.boldLabel );
					//if ( GUILayout.Button( "Import Peds" ) )
					//{
					//}

					break;

				case ImporterMode.Cars:
					
					EditorGUILayout.Space();
					EditorGUILayout.LabelField( "Cars", EditorStyles.boldLabel );

					colIndex = EditorGUILayout.Popup( "Collision", colIndex, importer.colNames );
					carIndex = EditorGUILayout.Popup( "Vehicle", carIndex, importer.carNames );
					handlingIndex = EditorGUILayout.Popup( "Handling", handlingIndex, importer.cfgNames );

					if ( GUILayout.Button( "Import Car" ) )
					{
						CarDef cardef = importer.carDefs[ carIndex ];
						importer.ImportHandling( importer.cfgNames[ handlingIndex ] );
						importer.ImportItem( cardef.id, false, importer.colNames[ colIndex ], "" );
						importer.Clear();
					}

					if ( GUILayout.Button( "Import All Cars" ) )
					{
						importer.ImportHandling( importer.cfgNames[ handlingIndex ] );
						importer.ImportCars( importer.colNames[ colIndex ] );
						importer.Clear();
					}
					
					if ( GUILayout.Button( "Import Handling" ) )
					{
						importer.ImportHandling( importer.cfgNames[ handlingIndex ] );
						importer.Clear();
					}

					break;

				case ImporterMode.Map:

					// MAPS

					EditorGUILayout.Space();
					EditorGUILayout.LabelField( "Maps", EditorStyles.boldLabel );
					EditorGUILayout.HelpBox( "Типы импортируемых файлов:\nIPL, IDE, DFF, TXD", MessageType.Info );
					mapIndex = EditorGUILayout.Popup( "Map Name", mapIndex, importer.iplNames );
					//pathsOnly = EditorGUILayout.Toggle( "Paths Only", pathsOnly );
					
					if ( GUILayout.Button( "Import Map" ) )
					{
						importer.ImportMapAtPath( importer.iplPaths[ mapIndex ] );
						importer.Clear();
					}

					break;
			}

			EditorGUILayout.EndVertical();
		}

		// Uncomment for cleaning non-responding progress bar
		//void Update()
		//{
		//	EditorUtility.ClearProgressBar();
		//}

		private void Clear()
		{
			importer.Clear();
		}

		private void PrintSelectedFiles()
		{
			GUILayout.Label( "Selected Files:", EditorStyles.boldLabel );

			foreach ( Object o in Selection.objects )
			{
				GUILayout.Label( AssetDatabase.GetAssetPath( o ) );
			}
		}

		//private void PrintHelpBox()
		//{
		//	EditorGUILayout.HelpBox(
		//		"Тип файла не является IWAD или PWAD.\n" +
		//		"Пожалуйста выберите файл с расширением *.wad",
		//		MessageType.Warning, true );
		//}
	}
}