﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.IO;

namespace GTA3
{
	class AnimationClipSettings
	{
		SerializedProperty m_Property;

		private SerializedProperty Get( string property ) { return m_Property.FindPropertyRelative( property ); }

		public AnimationClipSettings( SerializedProperty prop ) { m_Property = prop; }

		public float startTime { get { return Get( "m_StartTime" ).floatValue; } set { Get( "m_StartTime" ).floatValue = value; } }
		public float stopTime { get { return Get( "m_StopTime" ).floatValue; } set { Get( "m_StopTime" ).floatValue = value; } }
		public float orientationOffsetY { get { return Get( "m_OrientationOffsetY" ).floatValue; } set { Get( "m_OrientationOffsetY" ).floatValue = value; } }
		public float level { get { return Get( "m_Level" ).floatValue; } set { Get( "m_Level" ).floatValue = value; } }
		public float cycleOffset { get { return Get( "m_CycleOffset" ).floatValue; } set { Get( "m_CycleOffset" ).floatValue = value; } }

		public bool loopTime { get { return Get( "m_LoopTime" ).boolValue; } set { Get( "m_LoopTime" ).boolValue = value; } }
		public bool loopBlend { get { return Get( "m_LoopBlend" ).boolValue; } set { Get( "m_LoopBlend" ).boolValue = value; } }
		public bool loopBlendOrientation { get { return Get( "m_LoopBlendOrientation" ).boolValue; } set { Get( "m_LoopBlendOrientation" ).boolValue = value; } }
		public bool loopBlendPositionY { get { return Get( "m_LoopBlendPositionY" ).boolValue; } set { Get( "m_LoopBlendPositionY" ).boolValue = value; } }
		public bool loopBlendPositionXZ { get { return Get( "m_LoopBlendPositionXZ" ).boolValue; } set { Get( "m_LoopBlendPositionXZ" ).boolValue = value; } }
		public bool keepOriginalOrientation { get { return Get( "m_KeepOriginalOrientation" ).boolValue; } set { Get( "m_KeepOriginalOrientation" ).boolValue = value; } }
		public bool keepOriginalPositionY { get { return Get( "m_KeepOriginalPositionY" ).boolValue; } set { Get( "m_KeepOriginalPositionY" ).boolValue = value; } }
		public bool keepOriginalPositionXZ { get { return Get( "m_KeepOriginalPositionXZ" ).boolValue; } set { Get( "m_KeepOriginalPositionXZ" ).boolValue = value; } }
		public bool heightFromFeet { get { return Get( "m_HeightFromFeet" ).boolValue; } set { Get( "m_HeightFromFeet" ).boolValue = value; } }
		public bool mirror { get { return Get( "m_Mirror" ).boolValue; } set { Get( "m_Mirror" ).boolValue = value; } }
	}

	public partial class Importer /* .Animation */
	{
		public bool animUseKeyframeRotation = true;
		public bool animUseKeyframePosition = true;
		public bool animUseKeyframeScale = false;

		//-------------------------------------------------------------------
		// ANIMATION IMPORTER (IFP)
		//-------------------------------------------------------------------

		public void ImportAnimation( string ifpname, Transform playerHierarchy, WrapMode wrapMode )
		{
			if ( !playerHierarchy )
			{
				Debug.LogError( "Иерархия для анимации не выбрана" );
				return;
			}

			string ifppath = GetSrcDataPath( SrcDataType.Animation, ifpname );
			IFPReader r = new IFPReader( ifppath );
			r.Read();
			r.End();

			Dictionary<string, string> dict = playerHierarchy.RelativePathDict( true, "", true );
			foreach ( string name in dict.Keys )
				Log.WriteLine( name + "\t\t:\t" + dict[ name ] );

			float animStopTime = 0f;
			Progress_Start( "Creating animation assets" );
			for ( int i = 0; i < r.animations.Length; i++ )
			{
				bool cancel = Progress_Update( "Anims: " + i + " / " + r.animations.Length, ( float ) i / ( float ) r.animations.Length );
				if ( cancel )
					break;

				IFP_Animation ifp_anim = r.animations[ i ];
				AnimationClip clip = new AnimationClip();
				//AnimationUtility.SetAnimationType( clip, ModelImporterAnimationType.Generic );
				clip.name = ifp_anim.name;
				clip.wrapMode = wrapMode;

				bool skipAnimation = false;
				for ( int j = 0; j < ifp_anim.data.info.Length; j++ )
				{
					IFP_CPAN ifp_cpan = ifp_anim.data.info[ j ];
					string lowername = ifp_cpan.anim.objectName.ToLower();

					if ( !dict.ContainsKey( lowername ) )
					{
						Debug.Log( "Фрейм " + ifp_cpan.anim.objectName + " в иерархии отсутсвует. Анимация будет пропущена.\n" + ifp_anim.name );
						skipAnimation = true;
						break;
					}

					if ( ifp_cpan.keyframes == null )
						continue;

					AnimationCurve curveRotationX = new AnimationCurve();
					AnimationCurve curveRotationY = new AnimationCurve();
					AnimationCurve curveRotationZ = new AnimationCurve();
					AnimationCurve curveRotationW = new AnimationCurve();

					AnimationCurve curvePositionX = new AnimationCurve();
					AnimationCurve curvePositionY = new AnimationCurve();
					AnimationCurve curvePositionZ = new AnimationCurve();

					AnimationCurve curveScaleX = new AnimationCurve();
					AnimationCurve curveScaleY = new AnimationCurve();
					AnimationCurve curveScaleZ = new AnimationCurve();

					//Debug.Log( ifp_cpan.anim.objectName );

					for ( int k = 0; k < ifp_cpan.keyframes.Length; k++ )
					{
						IFP_Keyframe ifp_kf = ifp_cpan.keyframes[ k ];
						animStopTime = Mathf.Max( animStopTime, ifp_kf.time );

						Quaternion rot = MathUtils.LeftHand( ifp_kf.rot );
						Vector3 pos = MathUtils.LeftHand( ifp_kf.pos );
						Vector3 scale = MathUtils.LeftHand( ifp_kf.scale );

						curveRotationX.AddKey( ifp_kf.time, rot.x );
						curveRotationY.AddKey( ifp_kf.time, rot.y );
						curveRotationZ.AddKey( ifp_kf.time, rot.z );
						curveRotationW.AddKey( ifp_kf.time, rot.w );

						curvePositionX.AddKey( ifp_kf.time, pos.x );
						curvePositionY.AddKey( ifp_kf.time, pos.y );
						curvePositionZ.AddKey( ifp_kf.time, pos.z );

						curveScaleX.AddKey( ifp_kf.time, scale.x );
						curveScaleY.AddKey( ifp_kf.time, scale.y );
						curveScaleZ.AddKey( ifp_kf.time, scale.z );
					}

					string relpath = dict[ lowername ];

					if ( animUseKeyframeRotation && ifp_cpan.keyframeType.Contains( "R" ) )
					{
						clip.SetCurve( relpath, typeof( Transform ), "localRotation.x", curveRotationX );
						clip.SetCurve( relpath, typeof( Transform ), "localRotation.y", curveRotationY );
						clip.SetCurve( relpath, typeof( Transform ), "localRotation.z", curveRotationZ );
						clip.SetCurve( relpath, typeof( Transform ), "localRotation.w", curveRotationW );
					}

					if ( animUseKeyframePosition && ifp_cpan.keyframeType.Contains( "T" ) )
					{
						clip.SetCurve( relpath, typeof( Transform ), "localPosition.x", curvePositionX );
						clip.SetCurve( relpath, typeof( Transform ), "localPosition.y", curvePositionY );
						clip.SetCurve( relpath, typeof( Transform ), "localPosition.z", curvePositionZ );
					}

					if ( animUseKeyframeScale && ifp_cpan.keyframeType.Contains( "S" ) )
					{
						clip.SetCurve( relpath, typeof( Transform ), "localScale.x", curveScaleX );
						clip.SetCurve( relpath, typeof( Transform ), "localScale.y", curveScaleY );
						clip.SetCurve( relpath, typeof( Transform ), "localScale.z", curveScaleZ );
					}
				}

				if ( skipAnimation )
					continue;

				string path = GetAssetPath( ItemType.Unknown, "", AssetType.Anim, clip.name );
				AssetUtils.CreateAsset( clip, path );

				SerializedObject serializedClip = new SerializedObject( clip );
				AnimationClipSettings clipSettings = new AnimationClipSettings( serializedClip.FindProperty( "m_AnimationClipSettings" ) );
				clipSettings.loopTime = wrapMode == WrapMode.Loop;
				clipSettings.stopTime = animStopTime;
				serializedClip.ApplyModifiedProperties();
			}
			Progress_Stop();
		}

	}
}