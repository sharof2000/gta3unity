﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace GTA3
{
	public class COLReader : BaseReader
	{
		public COL_Model[] models;

		public COLReader( string path )
			: base( path )
		{
		}

		public void Read()
		{
			List<COL_Model> list = new List<COL_Model>();
			while ( !reader.EOF() )
			{
				COL_Model cm = ReadCollisionModel();
				if ( cm == null )
					break;
				list.Add( cm );
			}
			models = list.ToArray();
		}

		public COL_Model ReadCollisionModel()
		{
			string fourCC = ReadString( 4 );

			if ( fourCC != "COLL" )
			{
				Debug.Log( "Неподдерживаемая версия файла коллизий: " + fourCC );
				Log.WriteLine( "Неподдерживаемая версия файла коллизий: " + fourCC );
				return null;
			}

			COL_Model cm = new COL_Model();

			// HEADER

			cm.fourCC = fourCC;
			cm.size = reader.ReadInt32();
			cm.name = ReadString( COL.MODEL_NAME_SIZE );
			cm.gameModelIndex = reader.ReadInt16();
			cm.bounds = ReadBounds();

			Print( "fourCC: " + cm.fourCC );
			Print( "size: " + cm.size );
			Print( "collModelName: " + cm.name );
			Print( "gameModelIndex: " + cm.gameModelIndex );
			Print( "bounds: " + cm.bounds );
			
			// SPHERES

			int sphereCount = reader.ReadInt32();
			cm.spheres = new COL_Sphere[ sphereCount ];
			Print( "sphereCount: " + sphereCount );
			
			for ( int i = 0; i < sphereCount; i++ )
			{
				COL_Sphere s = new COL_Sphere();
				s.radius = reader.ReadSingle();
				s.center = ReadVector3();
				s.surface = ReadSurface();
				cm.spheres[ i ] = s;

				Print( "[" + i + "] " + s );
			}

			// UNKNOWNS
			
			// The unknown section was presumably planned to hold a set of lines, 
			// used for collisions with very thin objects (like railings). 
			// But this was never confirmed, nor is it used anywhere.

			int unknownCount = reader.ReadInt32();
			Print( "unknownCount: " + unknownCount );

			// BOXES

			int boxCount = reader.ReadInt32();
			cm.boxes = new COL_Box[ boxCount ];
			Print( "boxCount: " + boxCount );

			for ( int i = 0; i < boxCount; i++ )
			{
				COL_Box b = new COL_Box();
				b.min = ReadVector3();
				b.max = ReadVector3();
				b.surface = ReadSurface();
				cm.boxes[ i ] = b;

				Print( "[" + i + "] " + b );
			}

			try
			{
				// VERTICES

				int vertexCount = reader.ReadInt32();
				cm.vertices = new COL_Vertex[ vertexCount ];
				Print( "vertexCount: " + vertexCount );

				for ( int i = 0; i < vertexCount; i++ )
				{
					COL_Vertex v = new COL_Vertex();
					v.v = ReadVector3();
					cm.vertices[ i ] = v;

					Print( "[" + i + "] " + v );
				}

				// FACES

				int faceCount = reader.ReadInt32();
				cm.faces = new COL_Face[ faceCount ];
				Print( "faceCount: " + faceCount );

				for ( int i = 0; i < faceCount; i++ )
				{
					COL_Face f = new COL_Face();
					f.v1 = reader.ReadInt32();
					f.v2 = reader.ReadInt32();
					f.v3 = reader.ReadInt32();
					f.surface = ReadSurface();
					cm.faces[ i ] = f;

					Print( "[" + i + "] " + f );
				}
			}
			catch ( OutOfMemoryException ex )
			{
				return cm;
			}

			return cm;
		}

		public COL_Bounds ReadBounds()
		{
			COL_Bounds b = new COL_Bounds();
			
			b.radius = reader.ReadSingle();
			b.center = ReadVector3();
			b.min = ReadVector3();
			b.max = ReadVector3();
			
			//Print( "radius: " + b.radius );
			//Print( "center: " + b.center );
			//Print( "min: " + b.min );
			//Print( "max: " + b.max );
			
			return b;
		}

		public COL_Surface ReadSurface()
		{
			COL_Surface s = new COL_Surface();
			s.material = reader.ReadByte();
			s.flag = reader.ReadByte();
			s.unknown = reader.ReadByte();
			s.light = reader.ReadByte();
			return s;
		}
	}
}
