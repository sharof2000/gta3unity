﻿using UnityEngine;
using System;
using System.Collections;
using System.IO;
using System.Text;

namespace GTA3
{
	public class RWSReader : BaseReader
	{
		public const int RWS_STRING_SIZE = 32;
		public const int PAL8_COLORS = 256;
		public const int PAL8_CHANNELS = 4;

		public RWSReader( string path )
			: base( path )
		{
		}

		public void ReadAndPrintSections()
		{
			while ( !reader.EOF() )
			{
				RwsSectionHeader h = ReadSectionHeader();
				PrintSectionHeader( h );
					
				if ( h.length == 0 )
					break;

				if ( h.type == ( int ) RwsSectionID.ID_CLUMP )
					ReadAndPrintSections();
					
				reader.BaseStream.Seek( h.length, SeekOrigin.Current );
			}
		}

		public RwsSectionHeader OpenSection( RwsSectionID expectId, bool optSection = false )
		{
			long rewindPos = reader.BaseStream.Position;
			RwsSectionHeader h = ReadSectionHeader();
			if ( ( RwsSectionID ) h.type != expectId )
			{
				if ( !optSection )
					Debug.LogError( "Wrong section id: " + ( RwsSectionID ) h.type + ", expected id: " + expectId );
				reader.BaseStream.Seek( rewindPos, SeekOrigin.Begin );
				return null;
			}
			PrefixIncr();
			return h;
		}

		public void CloseSection( RwsSectionHeader h, bool skip = true )
		{
			if ( skip )
				reader.BaseStream.Seek( h.seekpos, SeekOrigin.Begin );
			PrefixDecr();
			Print( "</" + ( RwsSectionID ) h.type + ">" );
		}

		public RwsSectionHeader ReadSectionHeader()
		{
			RwsSectionHeader h = new RwsSectionHeader();
			h.type = reader.ReadInt32();
			h.length = reader.ReadInt32();
			h.version = reader.ReadInt32();
			h.seekpos = ( int ) reader.BaseStream.Position + ( int ) h.length;
			PrintSectionHeader( h );
			return h;
		}

		public string ReadStringSection()
		{
			RwsSectionHeader h = OpenSection( RwsSectionID.ID_STRING );
			string s = ReadString( h.length );
			Print( s );
			CloseSection( h );
			return s;
		}

		public string ReadString( int size = RWS_STRING_SIZE )
		{
			return base.ReadString( size );
		}

		public RwsSphere ReadSphere()
		{
			RwsSphere s = new RwsSphere();
			s.center = ReadVector3();
			s.radius = reader.ReadSingle();
			s.hasPosition = reader.ReadInt32();
			s.hasNormals = reader.ReadInt32();
			return s;
		}

		public RwsFace ReadFace()
		{
			RwsFace f = new RwsFace();
			f.v2 = reader.ReadInt16();
			f.v1 = reader.ReadInt16();
			f.alpha = reader.ReadInt16();
			f.v3 = reader.ReadInt16();
			return f;
		}

		// UTILS

		public void PrintSectionHeader( RwsSectionHeader h )
		{
			Print( "<" + ( RwsSectionID ) h.type + " length=\'" + h.length + "\' version=\'" + VersionToString( h.version ) + "\'>" );
		}

		public void PrintFace( RwsFace f )
		{
			Print( "[RwsFace] " + f.v1 + ", " + f.v2 + ", " + f.v3 + "  alpha: " + f.alpha );
		}
	}
}
