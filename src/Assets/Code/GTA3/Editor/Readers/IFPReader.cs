﻿using UnityEngine;
using UnityEditor;
using System;
using System.Collections;
using System.IO;
using System.Text;

namespace GTA3
{
	public class IFPReader : BaseReader
	{
		public const int STRING_SIZE = 28;
		public const int STRING_ALIGN = 4;
		public const int ID_SIZE = 4;

		public const int KR00_SIZE = 16;
		public const int KRT0_SIZE = 28;
		public const int KRTS_SIZE = 40;

		public IFP_Animation[] animations;

		public IFPReader( string path )
			: base( path )
		{
		}

		public bool Read()
		{
			IFP_Base h_anpk = OpenSection( "ANPK" );
			animations = ReadInfo_Animation();
			CloseSection( h_anpk );
			return true;
		}

		public IFP_Animation[] ReadInfo_Animation()
		{
			IFP_Base h_info = OpenSection( "INFO" );

			int count = reader.ReadInt32();
			string name = ReadString();

			Print( "Count: " + count );
			Print( "Name: " + name );

			//count = 1;

			IFP_Animation[] info = new IFP_Animation[ count ];

			Importer.Progress_Start( "Importing: " + filename );
			for ( int i = 0; i < count; i++ )
			{
				bool cancel = Importer.Progress_Update( "Animations readed: " + i + " / " + count, ( float ) i / ( float ) count );
				if ( cancel )
					break;

				info[ i ] = ReadAnimation();
			}
			Importer.Progress_Stop();

			CloseSection( h_info, false );
			return info;
		}

		public IFP_CPAN[] ReadInfo_CPAN()
		{
			IFP_Base h_info = OpenSection( "INFO" );

			int count = reader.ReadInt32();
			string name = ReadString();

			Print( "Count: " + count );
			Print( "Name: " + name );

			IFP_CPAN[] info = new IFP_CPAN[ count ];
			for ( int i = 0; i < count; i++ )
				info[ i ] = ReadCPAN();

			CloseSection( h_info, false );
			return info;
		}

		public IFP_Animation ReadAnimation()
		{
			IFP_Animation a = new IFP_Animation();
			a.name = ReadName();
			a.data = ReadDGAN();
			return a;
		}

		public IFP_DGAN ReadDGAN()
		{
			IFP_Base h_dgan = OpenSection( "DGAN" );
			IFP_DGAN dgan = new IFP_DGAN();
			dgan.info = ReadInfo_CPAN();
			CloseSection( h_dgan );
			return dgan;
		}

		public IFP_CPAN ReadCPAN()
		{
			IFP_Base h_cpan = OpenSection( "CPAN" );
			IFP_CPAN cpan = new IFP_CPAN();
			cpan.anim = ReadANIM();
			if ( cpan.anim.fakeFrameCount > 0 )
				cpan.keyframes = ReadKeyframes( cpan.anim.fakeFrameCount, out cpan.keyframeType );
			CloseSection( h_cpan );
			return cpan;
		}

		public IFP_ANIM ReadANIM()
		{
			IFP_Base h_anim = OpenSection( "ANIM" );

			IFP_ANIM anim = new IFP_ANIM();
			anim.objectName = ReadString( STRING_SIZE );
			anim.fakeFrameCount = reader.ReadInt32();
			anim.unknown = reader.ReadInt32();
			anim.nextSibling = reader.ReadInt32();
			anim.prevSibling = reader.ReadInt32();

			Print( "objectName: " + anim.objectName );
			Print( "fakeFrameCount: " + anim.fakeFrameCount );
			Print( "unknown: " + anim.unknown );
			Print( "nextSibling: " + anim.nextSibling );
			Print( "prevSibling: " + anim.prevSibling );

			CloseSection( h_anim );
			return anim;
		}

		
		public IFP_Keyframe[] ReadKeyframes( int frameCount, out string keyframeType )
		{
			IFP_Base h_kfrm = OpenSection( "KR00", "KRT0", "KRTS" );
			keyframeType = h_kfrm.id;

			//int frameCount = 0;
			//switch ( h_kfrm.id )
			//{
			//	case "KR00": frameCount = ( h_kfrm.offset - 4 ) / KR00_SIZE; break;
			//	case "KRT0": frameCount = ( h_kfrm.offset - 4 ) / KRT0_SIZE; break;
			//	case "KRTS": frameCount = ( h_kfrm.offset - 4 ) / KRTS_SIZE; break;
			//}

			IFP_Keyframe[] keyframes = new IFP_Keyframe[ frameCount ];
			for ( int i = 0; i < frameCount; i++ )
			{
				IFP_Keyframe kf = new IFP_Keyframe();

				switch ( h_kfrm.id )
				{
					case "KR00":
						kf.rot = ReadQuaternion();
						break;

					case "KRT0":
						kf.rot = ReadQuaternion();
						kf.pos = ReadVector3();
						break;

					case "KRTS":
						kf.rot = ReadQuaternion();
						kf.pos = ReadVector3();
						kf.scale = ReadVector3();
						break;
				}

				kf.time = reader.ReadSingle();

				Print( "[Keyframe: " + i + "]" );
				Print( "    rot: " + kf.rot );
				Print( "    pos: " + kf.pos );
				Print( "    scale: " + kf.scale );
				Print( "    time: " + kf.time );

				keyframes[ i ] = kf;
			}

			CloseSection( h_kfrm );
			return keyframes;
		}


		public string ReadName()
		{
			IFP_Base h_name = OpenSection( "NAME" );
			string name = ReadString();
			Print( name );
			CloseSection( h_name, false );
			return name;
		}


		public IFP_Base OpenSection( params string[] expectedIds )
		{
			IFP_Base h = ReadSectionHeader();

			bool correct = false;
			foreach ( string expected in expectedIds )
			{
				if ( h.id == expected )
				{
					correct = true;
					break;
				}
			}

			if ( !correct )
			{
				//Debug.LogError( "Unexpected section id: " + h.id + "\n" + filename );
				throw new UnityException( "Unexpected section id: " + h.id + "\n" + filename );
			}

			PrefixIncr();
			return h;
		}

		public void CloseSection( IFP_Base h, bool skip = true )
		{
			if ( skip && !reader.EOF() )
				reader.BaseStream.Seek( h.seekpos, SeekOrigin.Begin );
			PrefixDecr();
			Print( "</" + h.id + ">" );
		}

		public IFP_Base ReadSectionHeader()
		{
			IFP_Base h = new IFP_Base();
			h.id = ReadString( ID_SIZE );
			h.offset = reader.ReadInt32();
			h.seekpos = ( int ) reader.BaseStream.Position + ( int ) h.offset;
			PrintSectionHeader( h );
			return h;
		}

		public void PrintSectionHeader( IFP_Base h )
		{
			Print( "<" + h.id + " offset=\'" + h.offset + "\'>" );
		}

		public string ReadString( int size = -1 )
		{
			string s = "";
			if ( size == -1 )
			{
				size = STRING_SIZE;
				for ( int i = 0; i < size; i += STRING_ALIGN )
				{
					s += new string( reader.ReadChars( STRING_ALIGN ) );
					int nul = s.IndexOf( '\0' );
					if ( nul != -1 )
						return s.Substring( 0, nul );
				}
			}
			else
			{
				s = new string( reader.ReadChars( size ) );
				int nul = s.IndexOf( '\0' );
				if ( nul != -1 )
					return s.Substring( 0, nul );
			}
			return s;
		}
	}
}
