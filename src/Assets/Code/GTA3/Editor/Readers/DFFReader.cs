﻿using UnityEngine;
using System;
using System.Collections;
using System.IO;
using System.Text;

namespace GTA3
{
	public class DFFReader : RWSReader
	{
		public RwsFrame[] frameList;
		public RwsGeometry[] geometryList;

		public DFFReader( string path )
			: base( path )
		{
		}

		public void Read()
		{
			// [Clump]
			RwsSectionHeader h_clump = OpenSection( RwsSectionID.ID_CLUMP );

			// [Clump] -> [Struct]
			RwsSectionHeader h_struct = OpenSection( RwsSectionID.ID_STRUCT );
			int atomicCount = reader.ReadInt32();
			Print( "atomic count: " + atomicCount );
			CloseSection( h_struct );

			// [Clump] -> [FrameList]
			frameList = ReadFrameList();

			// [Clump] -> [GeometryList]
			geometryList = ReadGeometryList();

			// [Clump] -> [Atomic] {atomicCount}
			for ( int i = 0; i < atomicCount; i++ )
				ReadAtomic( i );

			CloseSection( h_clump );
		}

		public RwsFrame[] ReadFrameList()
		{
			// [FrameList]
			RwsSectionHeader h_framelist = OpenSection( RwsSectionID.ID_FRAMELIST );

			// [FrameList] -> [Struct]
			RwsSectionHeader h_struct = OpenSection( RwsSectionID.ID_STRUCT );
			int frameCount = reader.ReadInt32();
			RwsFrame[] frames = new RwsFrame[ frameCount ];
			for ( int i = 0; i < frameCount; i++ )
			{
				RwsFrame frame = new RwsFrame();
				frame.rotationX = ReadVector3();
				frame.rotationY = ReadVector3();
				frame.rotationZ = ReadVector3();
				frame.position = ReadVector3();
				frame.parentFrame = reader.ReadInt32();
				frame.matrixFlags = reader.ReadInt32();
				frame.linkedGeometry = -1;

				Print( "RwsFrame: " + i + "" );
				Print( "  rotationX: " + frame.rotationX );
				Print( "  rotationY: " + frame.rotationY );
				Print( "  rotationZ: " + frame.rotationZ );
				Print( "  position: " + frame.position );
				Print( "  parentFrame: " + frame.parentFrame );
				Print( "  matrixFlags: " + frame.matrixFlags );

				frames[ i ] = frame;
			}
			CloseSection( h_struct );

			// [FrameList] -> [Extension] {frameCount}
			for ( int i = 0; i < frameCount; i++ )
			{
				// [FrameList] -> [Extension]
				RwsSectionHeader h_ext = OpenSection( RwsSectionID.ID_EXTENSION );

				// [FrameList] -> [Extension] -> [Frame]
				RwsSectionHeader h_frame = OpenSection( RwsSectionID.ID_FRAME );
				string frameName = ReadString( h_frame.length );
				Print( frameName );
				frames[ i ].name = frameName;
				CloseSection( h_frame );

				CloseSection( h_ext );
			}

			CloseSection( h_framelist );
			return frames;
		}

		public RwsGeometry[] ReadGeometryList()
		{
			// [GeometryList]
			RwsSectionHeader h_geomlist = OpenSection( RwsSectionID.ID_GEOMETRYLIST, true );
			if ( h_geomlist != null )
			{
				// [GeometryList] -> [Struct]
				RwsSectionHeader h_struct = OpenSection( RwsSectionID.ID_STRUCT );
				int geomCount = reader.ReadInt32();
				Print( "geom count: " + geomCount );
				CloseSection( h_struct );

				RwsGeometry[] geoms = new RwsGeometry[ geomCount ];
				for ( int i = 0; i < geomCount; i++ )
					geoms[ i ] = ReadGeometry();

				CloseSection( h_geomlist );
				return geoms;
			}

			return null;
		}

		public RwsGeometry ReadGeometry()
		{
			// [Geometry]
			RwsSectionHeader h_geom = OpenSection( RwsSectionID.ID_GEOMETRY );

			// [Geometry] -> [Struct]
			RwsGeometry geom = ReadGeometryStruct();

			// [Geometry] -> [MaterialList]
			geom.materials = ReadMaterialList();

			// [Geometry] -> [Extension]
			RwsSectionHeader h_ext = OpenSection( RwsSectionID.ID_EXTENSION );
			{
				// [Geometry] -> [Extension] -> [BinMeshPlugin]
				RwsSectionHeader h_binmeshplg = OpenSection( RwsSectionID.ID_BINMESHPLUGIN );
				
				int faceType = reader.ReadInt32();
				int splitCount = reader.ReadInt32();
				int totalVertexCount = reader.ReadInt32();
				
				Print( "faceType: " + faceType );
				Print( "splitCount: " + splitCount );
				Print( "totalVertexCount: " + totalVertexCount );

				bool triangleStrip = faceType == ( int ) RwsGeometryFlags.GF_TRIANGLE_STRIP;

				for ( int i = 0; i < splitCount; i++ )
				{
					int vertexIndexCount = reader.ReadInt32();
					int materialIndex = reader.ReadInt32();

					Print( "{Split: " + i + "}" );
					Print( "  vertexIndexCount: " + vertexIndexCount );
					Print( "  materialIndex: " + materialIndex );

					int[] triangles = new int[ vertexIndexCount ];
					for ( int j = 0; j < vertexIndexCount; j++ )
					{
						triangles[ j ] = reader.ReadInt32();
						Print( "  " + triangles[ j ] );
					}

					geom.materials[ materialIndex ].triangles = triangles;
				}

				CloseSection( h_binmeshplg );
			}
			CloseSection( h_ext );

			CloseSection( h_geom );
			return geom;
		}

		public RwsGeometryHeader ReadGeometryHeader( RwsSectionHeader sh )
		{
			RwsGeometryHeader h = new RwsGeometryHeader();
			h.flags = reader.ReadInt16();
			h.uvCount = reader.ReadByte();
			h.geomNativeFlags = reader.ReadByte();
			h.faceCount = reader.ReadInt32();
			h.vertexCount = reader.ReadInt32();
			h.morphTargetCount = reader.ReadInt32();

			Print( "flags: " + ( ( RwsGeometryFlags ) h.flags ).FlagsToString() );
			Print( "uvCount: " + h.uvCount );
			Print( "geomNativeFlags: " + h.geomNativeFlags );
			Print( "faceCount: " + h.faceCount );
			Print( "vertexCount: " + h.vertexCount );
			Print( "morphTargetCount: " + h.morphTargetCount );

			if ( ( sh.version & 0xFFFF ) >= 0x0310 || sh.version == 0x0800FFFF )
			{
				h.ambientColor = ReadColor32();
				h.diffuseColor = ReadColor32();
				h.specularColor = ReadColor32();

				Print( "ambientColor: " + h.ambientColor );
				Print( "diffuseColor: " + h.diffuseColor );
				Print( "specularColor: " + h.specularColor );
			}
			return h;
		}

		public RwsGeometry ReadGeometryStruct()
		{
			RwsGeometry geom = new RwsGeometry();

			// [Geometry] -> [Struct]
			RwsSectionHeader h_struct = OpenSection( RwsSectionID.ID_STRUCT );

			// [Geometry] -> [Struct] -> GeometryHeader
			RwsGeometryHeader h = ReadGeometryHeader( h_struct );
			geom.h = h;

			// [Geometry] -> [Struct] -> GeometryData

			//------------------------------------------------------------
			// Vertex Colors
			//------------------------------------------------------------

			bool hasDiffuse = ( ( RwsGeometryFlags ) h.flags & RwsGeometryFlags.GF_DIFFUSE ) == RwsGeometryFlags.GF_DIFFUSE;
			if ( hasDiffuse )
			{
				geom.colors = new Color32[ h.vertexCount ];
				for ( int i = 0; i < h.vertexCount; i++ )
				{
					geom.colors[ i ] = ReadColor32();
					Print( "[Color32] " + geom.colors[ i ] );
				}
			}

			//------------------------------------------------------------
			// UV
			//------------------------------------------------------------
			
			bool hasUV1 = ( ( RwsGeometryFlags ) h.flags & RwsGeometryFlags.GF_UV1 ) == RwsGeometryFlags.GF_UV1;
			bool hasUV2 = ( ( RwsGeometryFlags ) h.flags & RwsGeometryFlags.GF_UV2 ) == RwsGeometryFlags.GF_UV2;

			if ( hasUV1 ) h.uvCount = Math.Max( h.uvCount, ( byte ) 1 );
			if ( hasUV2 ) h.uvCount = Math.Max( h.uvCount, ( byte ) 2 );

			for ( int uvIndex = 0; uvIndex < h.uvCount; uvIndex++ )
			{
				Vector2[] uv = new Vector2[ h.vertexCount ];
				for ( int i = 0; i < h.vertexCount; i++ )
				{
					uv[ i ] = ReadVector2();
					Print( "[UV] " + uv[ i ] );
				}

				if ( uvIndex == 0 )
					geom.uv1 = uv;
				else if ( uvIndex == 1 )
					geom.uv2 = uv;
				else if ( uvIndex == 2 )
					geom.uv3 = uv;
			}

			//------------------------------------------------------------
			// Faces
			//------------------------------------------------------------

			geom.faces = new RwsFace[ h.faceCount ];
			for ( int i = 0; i < h.faceCount; i++ )
			{
				geom.faces[ i ] = ReadFace();
				PrintFace( geom.faces[ i ] );
			}

			//------------------------------------------------------------
			// Bounding Sphere
			//------------------------------------------------------------
			
			geom.sphere = ReadSphere();

			//------------------------------------------------------------
			// Vertices
			//------------------------------------------------------------
			
			geom.vertices = new Vector3[ h.vertexCount ];
			for ( int i = 0; i < h.vertexCount; i++ )
			{
				geom.vertices[ i ] = ReadVector3();
				Print( "[Vertex] " + geom.vertices[ i ] );
			}

			//------------------------------------------------------------
			// Normals
			//------------------------------------------------------------
			
			bool hasNormals = ( ( RwsGeometryFlags ) h.flags & RwsGeometryFlags.GF_NORMALS ) == RwsGeometryFlags.GF_NORMALS;
			if ( hasNormals )
			{
				geom.normals = new Vector3[ h.vertexCount ];
				for ( int i = 0; i < h.vertexCount; i++ )
					geom.normals[ i ] = ReadVector3();
			}
			
			//------------------------------------------------------------

			CloseSection( h_struct );
			return geom;
		}

		public RwsMaterial[] ReadMaterialList()
		{
			// [MaterialList]
			RwsSectionHeader h_matlist = OpenSection( RwsSectionID.ID_MATERIALLIST );

			// [MaterialList] -> [Struct]
			RwsSectionHeader h_struct = OpenSection( RwsSectionID.ID_STRUCT );
			int materialCount = reader.ReadInt32();
			Print( "material count: " + materialCount );
			CloseSection( h_struct );

			// [MaterialList] -> [Material] {materialCount}
			RwsMaterial[] materials = new RwsMaterial[ materialCount ];
			for ( int i = 0; i < materialCount; i++ )
				materials[ i ] = ReadMaterial();

			CloseSection( h_matlist );
			return materials;
		}

		public RwsMaterial ReadMaterial()
		{
			// [Material]
			RwsSectionHeader h_mat = OpenSection( RwsSectionID.ID_MATERIAL );
			
			// [Material] -> [Struct]
			RwsSectionHeader h_struct = OpenSection( RwsSectionID.ID_STRUCT );
			RwsMaterial mat = ReadMaterialStruct();
			CloseSection( h_struct );
				
			// [Material] -> [Texture] {mat.textureCount}
			mat.textures = new RwsTexture[ mat.textureCount ];
			for ( int i = 0; i < mat.textureCount; i++ )
				mat.textures[ i ] = ReadTexture();

			// [Material] -> [Extension]
			RwsSectionHeader h_ext = OpenSection( RwsSectionID.ID_EXTENSION );
			CloseSection( h_ext );
			
			CloseSection( h_mat );
			return mat;
		}

		public RwsTexture ReadTexture()
		{
			RwsTexture tex = new RwsTexture();
			
			// [Texture]
			RwsSectionHeader h_tex = OpenSection( RwsSectionID.ID_TEXTURE );

			// [Texture] -> [Struct]
			RwsSectionHeader h_struct = OpenSection( RwsSectionID.ID_STRUCT );
			tex.filterFlags = reader.ReadInt16();
			tex.unknown = reader.ReadInt16();
			Print( "filterFlags: " + tex.filterFlags );
			Print( "unknown: " + tex.unknown );
			CloseSection( h_struct );

			// [Texture] -> [String] Texture Name
			tex.textureName = ReadStringSection();

			// [Texture] -> [String] Alpha Name
			tex.alphaName = ReadStringSection();

			// [Texture] -> [Extension]
			RwsSectionHeader h_ext = OpenSection( RwsSectionID.ID_EXTENSION );
			CloseSection( h_ext );
			
			CloseSection( h_tex );
			return tex;
		}

		public void ReadAtomic( int atomIndex )
		{
			// [Clump] -> [Atomic]
			RwsSectionHeader h_atom = OpenSection( RwsSectionID.ID_ATOMIC );

			// [Clump] -> [Atomic] -> [Struct]
			RwsSectionHeader h_struct = OpenSection( RwsSectionID.ID_STRUCT );
			RwsAtomic atom = ReadAtomicStruct();
			
			if ( frameList[ atom.frameIndex ].linkedGeometry != -1 )
				Debug.LogError( "Фрейму [" + atom.geometryIndex + "] назначено более одной геометрии" );

			frameList[ atom.frameIndex ].linkedGeometry = atom.geometryIndex;
			CloseSection( h_struct );

			// [Clump] -> [Atomic] -> [Extension]
			RwsSectionHeader h_ext = OpenSection( RwsSectionID.ID_EXTENSION );
			CloseSection( h_ext );

			CloseSection( h_atom );
		}

		public RwsAtomic ReadAtomicStruct()
		{
			RwsAtomic atom = new RwsAtomic();
			atom.frameIndex = reader.ReadInt32();
			atom.geometryIndex = reader.ReadInt32();
			atom.unknown1 = reader.ReadInt32();
			atom.unknown2 = reader.ReadInt32();
			PrintAtomicStruct( atom );
			return atom;
		}

		public void PrintAtomicStruct( RwsAtomic atom )
		{
			Print( "frameIndex: " + atom.frameIndex );
			Print( "geometryIndex: " + atom.geometryIndex );
			Print( "unknown1: " + atom.unknown1 );
			Print( "unknown2: " + atom.unknown2 );
		}

		public RwsMaterial ReadMaterialStruct()
		{
			RwsMaterial mat = new RwsMaterial();
			mat.unknown = reader.ReadInt32();
			mat.color = ReadColor32();
			mat.unknown2 = reader.ReadInt32();
			mat.textureCount = reader.ReadInt32();
			mat.ambient = reader.ReadSingle();
			mat.specular = reader.ReadSingle();
			mat.diffuse = reader.ReadSingle();
			PrintMaterialStruct( mat );
			return mat;
		}
		
		public void PrintMaterialStruct(RwsMaterial mat)
		{
			Print( "unknown: " + mat.unknown );
			Print( "color: " + mat.color );
			Print( "unknown2: " + mat.unknown2 );
			Print( "textureCount: " + mat.textureCount );
			Print( "ambient: " + mat.ambient );
			Print( "specular: " + mat.specular );
			Print( "diffuse: " + mat.diffuse );
		}
	}
}
