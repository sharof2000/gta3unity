﻿using UnityEngine;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace GTA3
{
	public class IDEParser : BaseParser
	{
		public IDE_Data ide;

		protected override string progressTitle { get { return "IDE parsing: " + filepath; } }

		public IDEParser()
		{
		}

		public override void Execute( string path )
		{
			//string idename = Path.GetFileNameWithoutExtension( path );
			//ide = IDE_Data.GetInstance( idename );
			//ide.Clear();
			
			ide = Importer.GetIDEInstance();
			base.Execute( path );
		}

		public override void ExecuteSection( ItemSection sect )
		{
			while ( tokenList[ currentTokenIndex ].type != TokenType.TOK_SECTION_END )
			{
				//if ( Importer.Progress_Update( progressInfo, progressRatio ) )
				//	break;

				ItemDef itemdef = ParseItemDef( sect, ide );
				if ( itemdef != null )
				{
					ide.Add( itemdef );
				}
				else
				{
					currentTokenIndex++;
				}
			}
		}

		public ItemDef ParseItemDef( ItemSection sect, IDE_Data ide )
		{
			ItemDef itemdef = null;
			switch ( sect )
			{
				case ItemSection.SEC_OBJS:
					ObjectDef objdef = new ObjectDef();
					objdef.id = ParseInteger();
					objdef.modelName = ParseString();
					objdef.txdName = ParseString();
					objdef.objCount = ParseInteger();
					// skip draw distances
					for ( int i = 0; i < objdef.objCount; i++ )
						ParseFloat();
					objdef.flags = ParseInteger();
					objdef.pathdefs = new List<PathDef>();
					itemdef = objdef;
					break;

				case ItemSection.SEC_TOBJ:
					ObjectDef tobjdef = new ObjectDef();
					tobjdef.id = ParseInteger();
					tobjdef.modelName = ParseString();
					tobjdef.txdName = ParseString();
					tobjdef.objCount = ParseInteger();
					// skip draw distances
					for ( int i = 0; i < tobjdef.objCount; i++ )
						ParseFloat();
					tobjdef.flags = ParseInteger();
					tobjdef.timeOn = ParseFloat();
					tobjdef.timeOff = ParseFloat();
					tobjdef.pathdefs = new List<PathDef>();
					itemdef = tobjdef;
					break;

				case ItemSection.SEC_PEDS:
					PedDef peddef = new PedDef();
					peddef.id = ParseInteger();
					peddef.modelName = ParseString();
					peddef.txdName = ParseString();
					peddef.defaultPedType = ParseString();
					peddef.behavior = ParseString();
					peddef.animGroup = ParseString();
					peddef.carsCanDriveMask = ParseHex();
					itemdef = peddef;
					break;

				case ItemSection.SEC_CARS:
					CarDef cardef = new CarDef();
					cardef.id = ParseInteger();
					cardef.modelName = ParseString();
					cardef.txdName = ParseString();
					cardef.carType = ParseString();
					cardef.handlingId = ParseString();
					cardef.gameName = ParseString();
					cardef.carClass = ParseString();
					cardef.frequency = ParseInteger();
					cardef.lvl = ParseInteger();
					cardef.compRules = ParseHex();
					if ( cardef.carType == "car" )
					{
						cardef.wheelId = ParseInteger();
						cardef.wheelScale = ParseFloat();
					}
					else if ( cardef.carType == "plane" )
					{
						cardef.planeLOD = ParseInteger();
					}
					itemdef = cardef;
					break;

				case ItemSection.SEC_PATH:
					PathDef pathdef = new PathDef();
					pathdef.pathType = ParseString();
					pathdef.id = ParseInteger();
					pathdef.modelName = ParseString();
					pathdef.nodes = new PathDefNode[ IDE.PATH_NODE_COUNT ];
					for ( int i = 0; i < IDE.PATH_NODE_COUNT; i++ )
					{
						PathDefNode node = new PathDefNode();
						node.type = ( PathDefNodeType ) ParseInteger();
						node.linkTo = ParseInteger();
						node.unknown1 = ParseInteger();
						node.position = MathUtils.LeftHand( ParseFloat3() );
						node.unknown2 = ParseInteger();
						node.lanes1 = ParseInteger();
						node.lanes2 = ParseInteger();
						pathdef.nodes[ i ] = node;
					}
					itemdef = pathdef;
					break;
			}

			if ( itemdef != null )
				itemdef.type = ItemSectionToType( sect, itemdef.id );

			return itemdef;
		}

		public static ItemType ItemSectionToType( ItemSection section, int id = -1 )
		{
			int len = IDE.itemGroupIds.GetLength( 0 );
			for ( int i = 0; i < len; i++ )
			{
				if ( id >= IDE.itemGroupIds[ i, 0 ] && id <= IDE.itemGroupIds[ i, 1 ] )
					return ItemGroupToType( ( ItemGroup ) i );
			}

			if ( section == ItemSection.SEC_OBJS )
				return ItemType.Object;

			if ( section == ItemSection.SEC_TOBJ )
				return ItemType.TimeObject;

			if ( section == ItemSection.SEC_PEDS )
				return ItemType.Ped;

			if ( section == ItemSection.SEC_CARS )
				return ItemType.Car;

			if ( section == ItemSection.SEC_PATH )
				return ItemType.Path;

			return ItemType.Unknown;
		}

		public static ItemType ItemGroupToType( ItemGroup type )
		{
			switch ( type )
			{
				case ItemGroup.Peds: return ItemType.Ped;
				case ItemGroup.Cars: return ItemType.Car;
				case ItemGroup.Wheels: return ItemType.Wheel;
				case ItemGroup.Weapons: return ItemType.Weapon;
				case ItemGroup.CarComps: return ItemType.CarComp;
				case ItemGroup.PedComps: return ItemType.PedComp;
				case ItemGroup.Misc: return ItemType.Misc;
			}

			return ItemType.Unknown;
		}
	}

}