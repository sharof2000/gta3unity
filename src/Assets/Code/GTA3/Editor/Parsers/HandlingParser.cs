﻿using UnityEngine;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace GTA3
{
	public class HandlingParser : BaseParser
	{
		public List<CarHandlingData> handlings;

		protected override string progressTitle { get { return "Handling parsing: " + filepath; } }

		public HandlingParser()
		{
			handlings = new List<CarHandlingData>();
		}

		public override void Execute( string path )
		{
			Log.WriteLine( path );

			filepath = path;
			filename = Path.GetFileName( path );

			Tokenize( path );

			Importer.Progress_Start( progressTitle );
			for ( currentTokenIndex = 0; currentTokenIndex < tokenList.Count; )
			{
				if ( Importer.Progress_Update( progressInfo, progressRatio ) )
					break;

				string id = ParseString();
				if ( id == "" )
					break;

				// CAR HANDLING

				CarHandlingData chd = new CarHandlingData();
				chd.id = id;
				chd.mass = ParseFloat();
				chd.dimensions = ParseFloat3();
				chd.com = ParseFloat3();
				chd.submergedRatio = ( float ) ParseInteger() / 100f;

				chd.tractionMultiplier = ParseFloat();
				chd.tractionLoss = ParseFloat();
				chd.tractionBias = ParseFloat();

				chd.transmission = new TransmissionData();
				chd.transmission.numGears = ParseInteger();
				chd.transmission.maxVelocity = ParseFloat();
				chd.transmission.engineAcceleration = ParseFloat();
				chd.transmission.driveType = StringToDriveType( ParseString() );
				chd.transmission.engineType = StringToEngineType( ParseString() );

				chd.brakeDeceleration = ParseFloat();
				chd.brakeBias = ParseFloat();

				chd.bABS = ( ParseInteger() == 1 );
				chd.steeringLock = ParseFloat();
				chd.suspensionForceLevel = ParseFloat();
				chd.suspensionDampingLevel = ParseFloat();
				chd.seatOffsetDistance = ParseFloat();
				chd.collisionDamageMultiplier = ParseFloat();
				chd.monetaryValue = ParseInteger();

				chd.suspensionUpperLimit = ParseFloat();
				chd.suspensionLowerLimit = ParseFloat();
				chd.suspensionBias = ParseFloat();

				chd.flags = ( CarFlags ) ParseHex();

				chd.frontLights = ( LightType ) ParseInteger();
				chd.rearLights = ( LightType ) ParseInteger();

				handlings.Add( chd );
			}
			Importer.Progress_Stop();
		}

		public static DriveType StringToDriveType( string driveType )
		{
			switch ( driveType )
			{
				case "F": return DriveType.FWD;
				case "R": return DriveType.RWD;
				case "4": return DriveType.FOUR;
			}
			throw new UnityException( "unknown drive type string: " + driveType );
		}

		public static EngineType StringToEngineType( string engineType )
		{
			switch ( engineType )
			{
				case "P": return EngineType.Petrol;
				case "D": return EngineType.Diesel;
				case "E": return EngineType.Electric;
			}
			throw new UnityException( "unknown engine type string: " + engineType );
		}
	}

}