﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;

public static class CollectionExtensions
{
	// LIST

	public static void AddUnique<T>( this List<T> list, T element )
	{
		if ( list.IndexOf( element ) == -1 )
			list.Add( element );
	}

	public static string JoinToString<T>( this List<T> list )
	{
		return string.Join( ", ", list.Select( x => x.ToString() ).ToArray() );
	}

	// ARRAY
	
	public static int IndexOf<T>( this T[] array, T element )
	{
		return Array.IndexOf( array, element );
	}

	public static int IndexOf( this string[] array, string s, bool ignoreCase )
	{
		for ( int i = 0; i < array.Length; i++ )
			if ( string.Compare( array[ i ], s, ignoreCase ) == 0 )
				return i;
		return -1;
	}

	public static string JoinToString<T>( this T[] array )
	{
		return string.Join( ", ", array.Select( x => x.ToString() ).ToArray() );
	}

	// IENUMERABLE

	public static string JoinToString<T>( this IEnumerable<T> e )
	{
		return string.Join( ", ", e.Select( x => x.ToString() ).ToArray() );
	}

}