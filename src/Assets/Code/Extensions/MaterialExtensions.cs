﻿using UnityEngine;
using System.Collections;

public static class MaterialExtensions {

	public static Color GetSpecular( this Material mat )
	{
		return mat.GetColor( "_SpecColor" );
	}

	public static void SetSpecular( this Material mat, Color color )
	{
		mat.SetColor( "_SpecColor", color );
	}
}
