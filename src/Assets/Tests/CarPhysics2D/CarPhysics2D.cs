﻿using UnityEngine;
using System.Collections;

public class CarPhysics2D : MonoBehaviour
{
	public float engineForce = 100f;
	public float dragCoeff = 0.05f;
	public float rrCoeff = 1.5f;
	
	//public Vector2 startVel = new Vector2( 10f, 20f );
	//public float startAngVel = 180;

	public float speed;
	public Vector2 Flong;

	public Graph graph_F_traction;
	public Graph graph_F_drag;
	public Graph graph_F_rr;
	public Graph graph_F_long;
	public Graph graph_F_drag_rr;


	// Use this for initialization
	void Start()
	{
		//rigidbody2D.velocity = startVel;
		//rigidbody2D.angularVelocity = startAngVel;
	}

	void FixedUpdate()
	{
		Vector2 dir = ( Vector2 ) transform.right;
		Vector2 p = rigidbody2D.position;
		Vector2 v = rigidbody2D.velocity;
		speed = v.magnitude;

		Vector2 Ftraction = dir * engineForce;
		Vector2 Fdrag = -dragCoeff * v * speed;
		Vector2 Frr = -rrCoeff * v;

		Flong = Ftraction + Fdrag + Frr;

		rigidbody2D.AddForce( Flong );

		if ( graph_F_traction )
			graph_F_traction.points.Add( new Vector2( Time.time, Ftraction.magnitude ) );
		if ( graph_F_drag )
			graph_F_drag.points.Add( new Vector2( Time.time, Fdrag.magnitude ) );
		if ( graph_F_rr )
			graph_F_rr.points.Add( new Vector2( Time.time, Frr.magnitude ) );
		if ( graph_F_long )
			graph_F_long.points.Add( new Vector2( Time.time, Flong.magnitude ) );
		if ( graph_F_drag_rr )
			graph_F_drag_rr.points.Add( new Vector2( Time.time, ( Fdrag + Frr ).magnitude ) );
	}

	void OnRenderObject()
	{
		Vector2 p = rigidbody2D.position;

		Debug.DrawLine( p, p + Flong );
	}
}
